package com.afkl.cases.df.logging;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	private Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Before("@annotation(com.afkl.cases.df.logging.AsyncLoggable)")
	public void entryLoggger(JoinPoint joinPoint) {
		logger.info("Entered: -------> " + joinPoint.getSignature() + "; Args: "
				+ Arrays.toString(joinPoint.getArgs()));

	}

	@AfterReturning(value = "@annotation(com.afkl.cases.df.logging.AsyncLoggable)", returning = "retVal")
	public void exitLoggger(JoinPoint joinPoint, Object retVal) {
		if (retVal == null) {
			logger.info("Exit OK: ------->" + joinPoint.getSignature() + "; Returning: null");
			return;
		}
		logger.info(
				"Exit OK: -------> " + joinPoint.getSignature() + "; Returning: " + retVal == null
						? null : retVal.toString());

	}

	@AfterThrowing(value = "@annotation(com.afkl.cases.df.logging.AsyncLoggable)", throwing = "e")
	public void exitLoggger(JoinPoint joinPoint, Exception e) {
		logger.error("Exit ERR: -------> " + joinPoint.getSignature() + "; Throwing: " + e);

	}
}