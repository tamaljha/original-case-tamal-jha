package com.afkl.cases.df.controllers.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.afkl.cases.df.controllers.BaseController;
import com.afkl.cases.df.logging.AsyncLoggable;
import com.afkl.cases.df.services.ErrorService;

@Controller
public class ErrorController extends BaseController {

	@Autowired
	private ErrorService errorService;

	@AsyncLoggable
	@RequestMapping(value = ERROR_PAGE_URI, method = RequestMethod.GET)
	public ModelAndView renderErrorPage(final ModelAndView mav, final HttpServletRequest request) {

		final int error_code = getHttpStatusCode(request);
		final String error_message = errorService.generateErrorMessage(error_code);

		mav.getModelMap().addAttribute(ERROR_MSG_LABEL, error_message);
		mav.setViewName(VIEW_NAME);
		return mav;
	}

	private int getHttpStatusCode(final HttpServletRequest request) {
		return (int) request.getAttribute(ERR_CODE_LABEL);
	}
}