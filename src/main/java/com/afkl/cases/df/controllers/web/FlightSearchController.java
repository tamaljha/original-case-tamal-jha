package com.afkl.cases.df.controllers.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping;

import com.afkl.cases.df.controllers.BaseController;
import com.afkl.cases.df.logging.AsyncLoggable;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.pagination.Pageable;
import com.afkl.cases.df.services.FlightSearchService;
import com.afkl.cases.df.services.LocationService;

@Controller
public class FlightSearchController extends BaseController {

	@Autowired
	BeanNameUrlHandlerMapping handlerMapping;

	@Autowired
	private FlightSearchService flightSearchService;

	@Autowired
	private LocationService locationService;

	@AsyncLoggable
	@RequestMapping(value = { HOME, ROOT }, produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = GET)

	public ModelAndView getAirportsPaginated(

			@RequestParam(value = LANG, defaultValue = "en") String lang,
			@RequestParam(value = SORT_BY, required = false) String sortBy,
			@RequestParam(value = DESCENDING_SORT, required = false) boolean descendingSort,
			Pageable<Location> pageable, ModelAndView mav) {

		List<Location> locations = locationService.getAllAirports(sortBy, descendingSort)
				.getEmbedded().getLocations();

		PagedResources<Resource<Location>> partition = pageable.partition(locations);
		locationService.getPreparedModel(sortBy, descendingSort, mav, partition);

		return mav;
	}

	@AsyncLoggable
	@RequestMapping(value = FARES, method = GET)
	public ModelAndView getFare(@RequestParam(value = ORIGIN, required = false) String origin,
			@RequestParam(value = DESTINATION, required = false) String destination,
			@RequestParam(value = CURRENCY, defaultValue = "EUR") String currency,
			ModelAndView mav) {

		mav.setViewName(VIEW_NAME);
		
		if (origin == null || destination == null) {
			return mav;
		}

		Future<Location> asyncOrigin = locationService.getAirportByCode(EN, origin);
		Future<Location> asyncDestination = locationService.getAirportByCode(EN, destination);
		Future<Fare> asyncFare = flightSearchService.getFare(origin, destination, currency);

		Fare fare = getFutureResult(asyncFare);
		fare.setOriginLocation(getFutureResult(asyncOrigin));
		fare.setDestinationLocation(getFutureResult(asyncDestination));
		mav.getModelMap().addAttribute(VIEW_NAME, fare);
		return mav;
	}

	@AsyncLoggable
	@RequestMapping(value = AIRPORTS_BY_KEY, method = GET)
	public Location getAirportByKey(@RequestParam(value = LANG, defaultValue = EN) String language,
			@PathVariable(value = AIRPORT_CODE) String airportCode) {

		Future<Location> asyncResult = locationService.getAirportByCode(language, airportCode);
		return getFutureResult(asyncResult);
	}

}