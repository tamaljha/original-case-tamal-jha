package com.afkl.cases.df.controllers;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.afkl.cases.df.constants.ApplicationConstants;

public abstract class BaseController implements ApplicationConstants {

	protected <T> T getFutureResult(Future<T> asyncFare) {
		try {
			int retries = 0;

			while (!asyncFare.isDone()) {
				Thread.sleep(RETRY_INTERVAL);
				retries++;
				if (retries > RETRY_ATTEMPTS) {
					return null;
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			return (T) asyncFare.get();
		} catch (ExecutionException e) {
			throw new IllegalArgumentException(e.getMessage());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

}
