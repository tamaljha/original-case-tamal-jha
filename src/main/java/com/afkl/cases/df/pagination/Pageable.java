package com.afkl.cases.df.pagination;

import static org.springframework.hateoas.PagedResources.wrap;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.PagedResources.PageMetadata;
import org.springframework.hateoas.Resource;
import org.springframework.web.client.HttpServerErrorException;

import com.afkl.cases.df.model.Location;
import com.google.common.collect.Lists;

public class Pageable<T> {

	private int page, size;

	public int getPage() {
		return page;
	}

	public int getSize() {
		return size;
	}

	public Pageable(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public PagedResources<Resource<Location>> partition(Collection<Location> original) {
		if (original == null || original.isEmpty()) {
			throw new HttpServerErrorException(NOT_FOUND,
					"Unable to partition results, no results available.");
		}
		final List<List<Location>> partitionedList = Lists.partition(
				original instanceof List ? (List<Location>) original : new ArrayList<>(original),
				size);
		try {
			List<Location> result = partitionedList.get(page == 0 ? page : page - 1);
			final PageMetadata metadata = new PageMetadata(size, page, original.size(),
					partitionedList.size());
			return wrap(result, metadata);
		} catch (IndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Requested page is out of bounds.");
		}
	}

}
