package com.afkl.cases.df.pagination;

import java.util.Optional;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.afkl.cases.df.constants.ApplicationConstants;
import com.afkl.cases.df.model.Location;

public class PageableHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return methodParameter.getParameterType().equals(Pageable.class);
	}

	@Override
	public Object resolveArgument(MethodParameter methodParameter,
			ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest,
			WebDataBinderFactory webDataBinderFactory) throws Exception {
		final int page = Optional
				.ofNullable(nativeWebRequest.getParameter(ApplicationConstants.PAGE))
				.map(p -> Math.abs(Integer.parseInt(p))).orElse(1);
		final int size = Optional
				.ofNullable(nativeWebRequest.getParameter(ApplicationConstants.SIZE))
				.map(s -> Math.abs(Integer.parseInt(s))).orElse(10);
		return new Pageable<Location>(page, size);
	}

}
