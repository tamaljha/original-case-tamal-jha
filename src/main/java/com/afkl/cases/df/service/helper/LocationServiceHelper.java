package com.afkl.cases.df.service.helper;

import static java.lang.String.format;

import java.util.Collections;
import java.util.Comparator;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.model.PagedLocations;
import com.afkl.cases.df.services.BaseService;

@Service
public class LocationServiceHelper extends BaseService {
	
	@Autowired
	private ServletContext servletContext;

	public PagedLocations getAirports(String sortBy, boolean descendingSort, PagedLocations pagedLocations) {
		sort(pagedLocations, sortBy, descendingSort);
		return pagedLocations;
	}
	
	public void prepareModel(String sortBy, boolean descendingSort, ModelAndView mav,
			PagedResources<Resource<Location>> partition) {
		
		mav.getModelMap().addAttribute(PARTITION, partition);
		mav.getModelMap().addAttribute(SORT_BY, sortBy);
		mav.getModelMap().addAttribute(DESCENDING_SORT, descendingSort); 
		mav.setViewName(VIEW_NAME);
		
		mav.getModelMap().addAttribute(FIRST, getFirstPage(sortBy, descendingSort, partition));
		mav.getModelMap().addAttribute(PREVIOUS,
				getPreviousPage(sortBy, descendingSort, partition));
		mav.getModelMap().addAttribute(NEXT, getNextPage(sortBy, descendingSort, partition));
		mav.getModelMap().addAttribute(LAST, getLastPage(sortBy, descendingSort, partition));
	}

	private String getFirstPage(String sortBy, boolean descendingSort,
			PagedResources<Resource<Location>> partition) {
		return format(LINK, servletContext.getContextPath(), 1, sortBy, descendingSort);
	}

	private String getPreviousPage(String sortBy, boolean descendingSort,
			PagedResources<Resource<Location>> partition) {
		return format(LINK, servletContext.getContextPath(), partition.getMetadata().getNumber() > 1
				? partition.getMetadata().getNumber() - 1 : 1, sortBy, descendingSort);
	}

	private String getNextPage(String sortBy, boolean descendingSort,
			PagedResources<Resource<Location>> partition) {
		return format(LINK, servletContext.getContextPath(),
				partition.getMetadata().getNumber() < partition.getMetadata().getTotalPages()
						? partition.getMetadata().getNumber() + 1
						: partition.getMetadata().getTotalPages(),
				sortBy, descendingSort);
	}

	private String getLastPage(String sortBy, boolean descendingSort,
			PagedResources<Resource<Location>> partition) {
		return format(LINK, servletContext.getContextPath(),
				partition.getMetadata().getTotalPages(), sortBy, descendingSort);
	}

	public void sort(PagedLocations pagedLocations, String sortBy, boolean descending) {

		Comparator<Location> compatatorOfChoice = null;
		if (sortBy != null) {
			switch (sortBy) {
			case NAME:
				if (descending)
					compatatorOfChoice = descendingByName;
				else
					compatatorOfChoice = ascendingByName;
				break;
			case CODE:
				if (descending)
					compatatorOfChoice = descendingById;
				else
					compatatorOfChoice = ascendingById;
				break;

			default:
				break;
			}
		}
		if (compatatorOfChoice != null)
			Collections.sort(pagedLocations.getEmbedded().getLocations(), compatatorOfChoice);
	}

	private Comparator<Location> descendingByName = new Comparator<Location>() {

		@Override
		public int compare(Location o1, Location o2) {
			return o2.getName().compareToIgnoreCase(o1.getName());
		}
	};

	private Comparator<Location> ascendingByName = new Comparator<Location>() {

		@Override
		public int compare(Location o1, Location o2) {
			return o1.getName().compareToIgnoreCase(o2.getName());
		}
	};

	private Comparator<Location> descendingById = new Comparator<Location>() {

		@Override
		public int compare(Location o1, Location o2) {
			return o2.getCode().compareToIgnoreCase(o1.getCode());
		}
	};

	private Comparator<Location> ascendingById = new Comparator<Location>() {

		@Override
		public int compare(Location o1, Location o2) {
			return o1.getCode().compareToIgnoreCase(o2.getCode());
		}
	};

}
