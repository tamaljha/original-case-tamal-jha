package com.afkl.cases.df.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public enum Currency {

    EUR, USD

}
