package com.afkl.cases.df.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Value;

@Value
@JsonDeserialize
public class Fare implements Serializable {

	@JsonIgnore
	private static final long serialVersionUID = 4675321914734157694L;

	@JsonProperty
	private double amount;

	@JsonProperty
	private Currency currency;

	@JsonProperty
	private Location originLocation;

	@JsonProperty
	private Location destinationLocation;

	public Fare(double amount, Currency currency, Location origin, Location destination) {
		super();
		this.amount = amount;
		this.currency = currency;
		this.originLocation = origin;
		this.destinationLocation = destination;
	}

	public Fare() {
		super();
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Location getOriginLocation() {
		return originLocation;
	}

	public void setOriginLocation(Location originLocation) {
		this.originLocation = originLocation;
	}

	public Location getDestinationLocation() {
		return destinationLocation;
	}

	public void setDestinationLocation(Location destinationLocation) {
		this.destinationLocation = destinationLocation;
	}

	@Override
	public String toString() {
		return "Fare [amount=" + amount + ", currency=" + currency + ", originLocation="
				+ originLocation + ", destinationLocation=" + destinationLocation + "]";
	}

}
