package com.afkl.cases.df.model;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Value;

@Value
@JsonDeserialize
@JsonInclude(NON_NULL)
public class Location implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7830615997658050649L;

	private String code;
	private String name;
	private String description;
	private Coordinates coordinates;
	private Location parent;
	private Set<Location> children;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	public Location getParent() {
		return parent;
	}

	public void setParent(Location parent) {
		this.parent = parent;
	}

	public Set<Location> getChildren() {
		return children;
	}

	public void setChildren(Set<Location> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "Location [code=" + code + ", name=" + name + ", ...]";
	}
}
