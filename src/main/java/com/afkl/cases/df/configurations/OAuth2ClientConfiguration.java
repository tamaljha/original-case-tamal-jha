package com.afkl.cases.df.configurations;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class OAuth2ClientConfiguration extends WebMvcConfigurerAdapter {
	@Value("${config.oauth2.accessTokenUri}")
	private String accessTokenUri;

	@Value("${config.oauth2.userAuthorizationUri}")
	private String userAuthorizationUri;

	@Value("${config.oauth2.clientID}")
	private String clientID;

	@Value("${config.oauth2.clientSecret}")
	private String clientSecret;

	@Value("${config.oauth2.grantType}")
	private String grantType;

	@Bean
	public OAuth2RestOperations restTemplate() {
		AccessTokenRequest atr = new DefaultAccessTokenRequest();

		return new OAuth2RestTemplate(withOAuth2Authentication(),
				new DefaultOAuth2ClientContext(atr));
	}

	private OAuth2ProtectedResourceDetails withOAuth2Authentication() {
		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();

		resource.setAccessTokenUri(accessTokenUri);
		resource.setClientId(clientID);
		resource.setClientSecret(clientSecret);
		resource.setAccessTokenUri(accessTokenUri);
		resource.setScope(Arrays.asList("read"));
		resource.setGrantType(grantType);

		return resource;
	}
}
