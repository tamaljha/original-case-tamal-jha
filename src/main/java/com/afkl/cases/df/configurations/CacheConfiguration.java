package com.afkl.cases.df.configurations;

import java.util.Arrays;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.afkl.cases.df.constants.ApplicationConstants;

@Configuration
@EnableCaching
public class CacheConfiguration {

	@Bean
	public CacheManager cacheManager() {
		SimpleCacheManager cacheManager = new SimpleCacheManager();
		cacheManager.setCaches(
				Arrays.asList(new ConcurrentMapCache(ApplicationConstants.ALL_AIRPORTS_CACHE_NAME),
						new ConcurrentMapCache(ApplicationConstants.AIRPORT_BY_CODE_CACHE_NAME),
						new ConcurrentMapCache(ApplicationConstants.FARE_CACHE_NAME)));
		return cacheManager;
	}

	@Bean(name = ApplicationConstants.KEY_GENERATOR_BEAN_NAME)
	public KeyGenerator keyGenerator() {
		return new SimpleKeyGenerator();
	}

}
