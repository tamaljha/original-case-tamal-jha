package com.afkl.cases.df.configurations;

import java.util.List;

import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.afkl.cases.df.constants.ApplicationConstants;
import com.afkl.cases.df.pagination.PageableHandlerMethodArgumentResolver;

@Configuration
@EnableOAuth2Client
@EnableAsync
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/index.html")
				.addResourceLocations("classpath:/static/index.html");
		registry.addResourceHandler("/index.js")
				.addResourceLocations("classpath:/static/js/index.js");
		registry.addResourceHandler("/style.css")
				.addResourceLocations("classpath:/static/css/style.css");
		registry.addResourceHandler("/popup.js")
				.addResourceLocations("classpath:/static/css/style.css");
		registry.addResourceHandler("/destination-image.jpg")
				.addResourceLocations("classpath:/static/images/destination-image.jpg");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new PageableHandlerMethodArgumentResolver());
	}

	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {
		return (container -> {
			final ErrorPage errorPage = new ErrorPage(ApplicationConstants.ERROR_PAGE_URI);
			container.addErrorPages(errorPage);
		});
	}

}
