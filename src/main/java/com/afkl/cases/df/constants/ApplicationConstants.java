package com.afkl.cases.df.constants;

public interface ApplicationConstants {
	
	//GENERAL LABELS
	public static final String EN = "en";
	public static final String LANG = "lang";
	public static final String CURRENCY = "currency";
	public static final String SIZE = "size";
	public static final String PAGE = "page";
	public static final String AIRPORT_CODE = "key";
	public static final String DESTINATION = "destination";
	public static final String ORIGIN = "origin";
	public static final String SORT_BY = "sortBy";
	public static final String PARTITION = "partition";
	public static final String DESCENDING_SORT = "descendingSort";
	public static final String FIRST = "first";
	public static final String PREVIOUS = "previous";
	public static final String NEXT = "next";
	public static final String LAST = "last";

	//URIS
	public static final String ERROR_PAGE_URI="/errors";
	public static final String LINK = "%s?page=%d&sortBy=%s&descending=%b";
	public static final String FARES = "/fares";
	public static final String ROOT = "/";
	public static final String HOME = "/home";
	public static final String AIRPORTS_BY_KEY = "/airports/{key}";
	public static final String API = "/api";

	//VIEW NAMES
	public static final String VIEW_NAME = "airports";

	public static final String ERROR_MSG_LABEL = "error_message";
	public static final String ERR_CODE_LABEL = "javax.servlet.error.status_code";

	//CACHE NAMES AND LABELS
	public static final String FARE_CACHE_NAME = "fare";
	public static final String AIRPORT_BY_CODE_CACHE_NAME = "airports";
	public static final String PAGINATED_AIRPORTS_CACHE_NAME = "paginatedAirports";
	public static final String ALL_AIRPORTS_CACHE_NAME = "allAirports";
	public static final String KEY_GENERATOR_BEAN_NAME = "keyGenerator";

	//ASYNC RELATED CONSTANTS
	public static final int RETRY_INTERVAL = 500;
	public static final int RETRY_ATTEMPTS = 60;

	//RESPONSE CODES
	public static final String _500 = "500";
	public static final String _404 = "404";
	public static final String _401 = "401";
	public static final String _400 = "400";

	public static final String NAME = "name";
	public static final String CODE = "code";

	public static final int MAX_SIZE = 1050;
	public static final int START_PAGE = 1;
}
