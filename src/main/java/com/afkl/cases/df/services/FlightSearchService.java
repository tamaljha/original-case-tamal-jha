package com.afkl.cases.df.services;

import java.net.URI;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.afkl.cases.df.logging.AsyncLoggable;
import com.afkl.cases.df.model.Fare;

@Service
public class FlightSearchService extends BaseService {

	@Autowired
	private OAuth2RestOperations restOperation;

	@Async
	@Cacheable(value = FARE_CACHE_NAME, keyGenerator = KEY_GENERATOR_BEAN_NAME)
	@AsyncLoggable
	public Future<Fare> getFare(String origin, String destination, String currency) {
		URI uri = UriComponentsBuilder.fromHttpUrl(faresURI).pathSegment(origin, destination)
				.queryParam(CURRENCY, currency).build().encode().toUri();

		return new AsyncResult<Fare>(restOperation.getForObject(uri, Fare.class));
	}

}
