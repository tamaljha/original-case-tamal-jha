package com.afkl.cases.df.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestOperations;

import com.afkl.cases.df.constants.ApplicationConstants;

public class BaseService implements ApplicationConstants {

	@Autowired
	protected OAuth2RestOperations restOperation;

	@Value("${config.oauth2.baseResourceURI}" + "${config.oauth2.faresResourceURI}")
	protected String faresURI;

	@Value("${config.oauth2.baseResourceURI}" + "${config.oauth2.airportsResourceURI}")
	protected String airportsURI;

	@Value("${config.oauth2.airportsResourceURI}")
	protected String airportsPartURI;

}
