package com.afkl.cases.df.services;

import java.net.URI;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.afkl.cases.df.logging.AsyncLoggable;
import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.model.PagedLocations;
import com.afkl.cases.df.service.helper.LocationServiceHelper;

@Service
public class LocationService extends BaseService {

	@Autowired
	LocationServiceHelper helper;


	@AsyncLoggable
	@Cacheable(value = ALL_AIRPORTS_CACHE_NAME, keyGenerator = KEY_GENERATOR_BEAN_NAME)
	public PagedLocations getAllAirports(String sortBy, boolean descendingSort) {
		URI uri = UriComponentsBuilder.fromHttpUrl(airportsURI).queryParam(PAGE, START_PAGE)
				.queryParam(SIZE, MAX_SIZE).build().encode().toUri();
		PagedLocations pagedLocations = restOperation.getForObject(uri, PagedLocations.class);
		return helper.getAirports(sortBy, descendingSort, pagedLocations);
	}


	@Async
	@AsyncLoggable
	@Cacheable(value = AIRPORT_BY_CODE_CACHE_NAME, keyGenerator = KEY_GENERATOR_BEAN_NAME)
	public Future<Location> getAirportByCode(String language, String airportCode) {
		URI uri = UriComponentsBuilder.fromHttpUrl(airportsURI).queryParam(LANG, language)
				.pathSegment(airportCode).build().encode().toUri();

		Location forObject = restOperation.getForObject(uri, Location.class);
		AsyncResult<Location> asyncResult = new AsyncResult<Location>(forObject);
		return asyncResult;
	}

	public void getPreparedModel(String sortBy, boolean descendingSort, ModelAndView mav,
			PagedResources<Resource<Location>> partition) {
		helper.prepareModel(sortBy, descendingSort, mav, partition);
	}

}
