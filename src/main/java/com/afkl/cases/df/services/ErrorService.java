package com.afkl.cases.df.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:httpErrorCodes.properties")
public class ErrorService extends BaseService {

	@Autowired
	private Environment env;

	public String generateErrorMessage(final int error_code) {
		String message = "";
		switch (error_code) {
		case 400:
			message = env.getProperty(_400);
			break;
		case 401:
			message = env.getProperty(_401);
			break;
		case 404:
			message = env.getProperty(_404);
			break;
		case 500:
			message = env.getProperty(_500);
			break;
		}
		return message;
	}
}
